pragma solidity ^0.5.0;

import "./Token.sol";
import './SafeMath.sol';

// //TODO
// Deposit Ether 
// Withdraw Ether
// Init Call
// End Call


contract Platform {
		using SafeMath for uint;

		//address public feeAccount;
		uint256 public feePercent;
		address constant ETHER = address(0);
		// uint256 public lockedUntil;
		// uint256 public secondsBooked;
		uint256 public callCount;
		uint256 public postCount;
		uint256 public totalPrice;
		uint256 public _secondsToPay;
		uint256 public donation = 42000000000000;
		uint256 public donationCount;

		mapping(address => uint256) public accounts;//map ether balances

		mapping(address => mapping(address=> uint256)) public tokens;
		// mapping(address => )
		mapping(uint256 => Call) public calls;
		
		mapping(address => Check) public verify;

		mapping(uint256 => bool) public callEnded;

		mapping(uint => Post) public posts;


		
		event Deposit(address token, address user, uint256 amount, uint256 balance);
		
		event Withdraw(address token, address user, uint256 amount, uint256 balance);
		
		event Donation( uint256 id, address donator, address beneficiary, uint256 timestamp);

		event StartCall(
				uint256 id,
				address caller, 
				address callee, 
				uint256 timestamp
				);

		event EndCall (
				uint256 id,
				address caller, 
				address callee, 
				uint256 timestamp
				);
		event TradeClosed (
				uint256 id,
				address caller, 
				address callee, 
				uint256 timestamp,
				uint256 payment,
				uint256 fee);

	
		event PostCreated (
        		uint256 id,
        		string content,
        		string category,
        		uint256 priceHour,
        		address author
    			);
		
		struct Call{
		uint256 id;
		address caller;
		address callee;
		uint256 timestamp;
		uint256 price;
		}

		struct Check {
		uint256 _id;
		bool _startCall;
		bool _endCall;
		}

		struct Post {
        uint id;
        string content;
        string category;
        uint256 priceHour;
        address author;
    	}
	

		constructor (uint256 _feePercent) public {
			feePercent = _feePercent;
			// lockedUntil= now.add(secondsBooked);
		}

		function() external {
			revert();
		}

		function depositEther() payable public {
			
			tokens[ETHER][msg.sender] = tokens[ETHER][msg.sender].add(msg.value);

			emit Deposit (ETHER, msg.sender, msg.value, tokens[ETHER][msg.sender]);

		}

		function withdrawEther(uint256 _amount) public {
			Check storage _verify = verify[msg.sender]; 

			require (_verify._startCall == false);
			
			require(tokens[ETHER][msg.sender] >= _amount);
	

			tokens[ETHER][msg.sender] = tokens[ETHER][msg.sender].sub(_amount);
			msg.sender.transfer(_amount);


			emit Withdraw (ETHER, msg.sender, _amount, tokens[ETHER][msg.sender]);
		}

		function depositToken(address _token, uint _amount) public {
        require(_token != ETHER);
        
        require(Token(_token).transferFrom(msg.sender, address(this), _amount));
        tokens[_token][msg.sender] = tokens[_token][msg.sender].add(_amount);
        emit Deposit(_token, msg.sender, _amount, tokens[_token][msg.sender]);
    	}

    	 function withdrawToken(address _token, uint256 _amount) public {
        Check storage _verify = verify[msg.sender]; 
        require(_token != ETHER);
        require (_verify._startCall == false);
        require(tokens[_token][msg.sender] >= _amount);
        tokens[_token][msg.sender] = tokens[_token][msg.sender].sub(_amount);
        require(Token(_token).transfer(msg.sender, _amount));
        emit Withdraw(_token, msg.sender, _amount, tokens[_token][msg.sender]);
    }	


		function balanceOf (address _token, address _user) public view returns (uint256) {

			return tokens[_token][_user];
		}

		//no sirve
		function balanceEther (address _user) public view returns (uint256) {

			return accounts[_user];
		}

		

		function startCall (address _callee, uint256 _priceInMinutes, uint256 _secondsBooked) public {
			Check storage _verify = verify[msg.sender];
			require (_verify._startCall == false);

			totalPrice = _priceInMinutes.mul(_secondsBooked).div(60);
			require (tokens[ETHER][msg.sender] >= totalPrice);
			
			callCount = callCount.add(1);

			calls[callCount] = Call(callCount, msg.sender, _callee, now, _priceInMinutes);

			verify[msg.sender] = Check (callCount, true, false);

			emit StartCall (callCount, msg.sender, _callee, now);


		}

		
		function endCall (uint256 _id) public {
		 	 Check storage _verify = verify[msg.sender];
			require (_verify._startCall == true);

		 	 Call storage _call = calls[_id];

		 	 require (_id > 0 && _id <= callCount);
		 	 require (address(_call.caller) == msg.sender); 
		 	 require (_call.id == _id);



			emit EndCall (_call.id, msg.sender, _call.callee, now);

			_secondsToPay = now.sub(_call.timestamp);

			
			uint256 _feeAmount = _secondsToPay.mul(_call.price).mul(feePercent).div(100).div(60);
				
			uint256 _totalValue = _secondsToPay.mul(_call.price).div(60);
			 	
        	callEnded[_id] = true; 

		 	verify[msg.sender] = Check(_id, false, true);

		 	calls[_id]= Call (_id, msg.sender, _call.callee, now, _call.price); 
    // falta decidir que hacer con el _feeAmount, es decir donde lo mandamos
			
			tokens[ETHER][_call.caller] = tokens[ETHER][_call.caller].sub(_totalValue);
			
			tokens[ETHER][_call.callee] = tokens[ETHER][_call.callee].add(_totalValue).sub(_feeAmount);

			


		 	emit TradeClosed (_call.id, _call.caller, _call.callee, now, _totalValue, _feeAmount);
			

			
			}

			
 
   		 function transfer(address beneficiary) public {
    	require (tokens[ETHER][beneficiary] > donation);

    	tokens[ETHER][beneficiary] = tokens[ETHER][beneficiary].sub(donation);
    	tokens[ETHER][msg.sender] = tokens[ETHER][msg.sender].add(donation); 
    	//no estoy seguro si se puede embalar el sistema con esto, linea arriba.

    	donationCount = donationCount.add(1);

        emit Donation(donationCount, msg.sender, beneficiary, now);
   		}

   		function createPost(string memory _content, string memory _category, uint256 _priceHour) public {
        // Require valid content
        require(bytes(_content).length > 0);
        require(bytes(_category).length > 0);
        // Increment the post count
        postCount =postCount.add(1);
        // Create the post
        posts[postCount] = Post(postCount, _content, _category, _priceHour, msg.sender);
        // Trigger event
        emit PostCreated(postCount, _content, _category, _priceHour, msg.sender);
    	}

		// function _closeTrade(uint256 _id, uint256 _payment, uint256 _price) internal {
		// 	Call storage _call = calls[_id];

		// uint256 _feeAmount = _secondsToPay.mul(_call.price).mul(feePercent).div(100).div(60);
				
		// 	uint256 _totalValue = _secondsToPay.mul(_call.price).div(60);

		// 	// falta decidir que hacer con el _feeAmount, es decir donde lo mandamos
			
		// 	tokens[ETHER][_call.caller] = tokens[ETHER][_call.caller].sub(_totalValue);
		// 	tokens[ETHER][_call.callee] = tokens[ETHER][_call.callee].add(_totalValue).sub(_feeAmount);

		// 	emit TradeClosed (_call.id, _call.caller, _call.callee, now, _totalValue, _feeAmount); 

		//  }
}