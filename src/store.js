
export const online = [true,false]

export const categories = [
  "Lawyer",
  "Psycologist",
  "Blockchain",
  "Fitness",
  "Finance"
]

export const advisors = [
  {
    id: "0xFFcf8FDEE72ac11b5c542428B35EEF5769C409f0",
    online: false,
    priceHour: "40",
    name: "yamal",
    description: "Specialist in divorce and crypto",
    categories: "Lawyer"
  },
  {
    id: "0x22d491Bde2303f2f43325b2108D26f1eAbA1e32b",
    online: true,
    priceHour: "50",
    name: "ale",
    description: "Voip Expert",
    categories: "Blockchain"
  },
  {
    id: "0xE11BA2b4D45Eaed5996Cd0823791E0C93114882d",
    online: true,
    priceHour: "30",
    name: "johana",
    description: "Professional trainer, can design customized training",
    categories: "Fitness"
  },
  {
    id: "0xd03ea8624C8C5987235048901fB614fDcA89b117",
    online: true,
    priceHour: "90",
    name: "paolo",
    description: "Accounting expert, can help with UK company set up",
    categories: "Finance"
  }
]


// identity: Identity
// _id: "02a62d707d67852f532b1a9339893660817491a6ced0410516bb331bdaf0a15bca"
// _provider: Identities {_keystore: Keystore, _signingKeystore: Keystore, _knownIdentities: LRU}
// _publicKey: "04b702802087b938ab3b0905038db1542c183fb76b834f4d8f50b99d2ec539ba57c6db85d2315347cec9a89c52f5ccf19ef1806a61681886444d6a13240c4c65de"
// _signatures:
// id: "304402201d157fb777a193174456435fd084818c27ea02691b855cbdf26822311d49ed62022012c4fe346d298b11726bd82a8111c4dbf06e4abbf98a85c28bce349e9e0a31eb"
// publicKey: "3045022100a8af139df958147a23dd9b6c28cd9a746b52afd5b6742e2706c36a220fc3016e0220697b3b109f4d13ce1c3aeb8ba53dcf0821b15a5374e13a9852249975690ff038"