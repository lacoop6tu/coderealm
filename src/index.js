import React from "react";
import { render } from "react-dom";
import App from "./Components/App";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple'
import yellow from '@material-ui/core/colors/yellow'
import blue from '@material-ui/core/colors/blue'
import indigo from '@material-ui/core/colors/indigo'
//import Orbit from "./Components/Orbit"
//console.log(drizzle, drizzleState)
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
// import DateFnsUtils from '@date-io/date-fns'
import MomentUtils from '@date-io/moment';
const theme = createMuiTheme({
	  palette: {
      primary: {
		main: indigo[500],
		light: yellow.A200,
      },
	  secondary: {
		  main: purple.A200,

	  },
	  background: {
		  paper: blue[500],
		  new: blue[500],
		  
	  },

	  type: 'light'
	},
	
	
   });
  console.log(theme)
render(
	
	<MuiThemeProvider theme={theme}>
	<MuiPickersUtilsProvider utils={MomentUtils}>
	<App/> 
	</MuiPickersUtilsProvider>
	</MuiThemeProvider>

	
	,document.getElementById("root"));

	
