export const ETHER_ADDRESS= '0x0000000000000000000000000000000000000000'
export const GREEN = 'success'
export const RED = 'danger'
export const DECIMALS = (10**18)

//shortcut to avoid passing around web3 connection
export const ether = (wei)=>{ 
	if(wei) {
		return (wei / DECIMALS)
	}
}
//tokens and ether have same decimals resolution.
export const tokens = ether

export const formatBalance = (balance)=>{
	const precision = 100
	 balance = ether(balance)
	 balance = Math.round(balance * precision)/precision
	return balance
}

export const toWei = (value) =>{
	if (value) {
		return (value*DECIMALS)
	}
}

export const fromWei = (value) =>{
	if (value) {
		return (value/DECIMALS)
	}
}
