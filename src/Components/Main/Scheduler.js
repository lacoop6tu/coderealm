
import React, { Fragment, useState } from "react";
import { DateTimePicker } from "@material-ui/pickers";
import { withContext } from "../../context"
import {
  // Grid,
  // Paper,
  // Typography,
  // List,
  // ListItem,
  // ListItemText,
  // ListItemSecondaryAction,
  // IconButton,
  // Card,
  // CardActionArea,
  // CardContent,
  // CardMedia,
  // CardActions,
  Button
  
} from "@material-ui/core"
import GenerateRandomCode from 'react-random-code-generator'



const Scheduler = (
  advisor,
  advisorAccount,
  myAccount,
  dbBookings
  ) => {

  console.log("advisorAccount",advisor.myAccount)
  const [selectedDate, handleDateChange] = useState(new Date());


  const handleBooking = async (advisor,selectedDate) => {
    const randomCode = GenerateRandomCode.TextNumCode(6, 6);    
      const idClient = advisor.myAccount
        const idAdvisor = advisor.advisor.id
        const selectedDateBooked = selectedDate.toString()
        const data = {idClient, idAdvisor, selectedDateBooked, randomCode}
    const booking = { idClient, idAdvisor, selectedDateBooked, randomCode}
    const prova = "12345678"
    const prova2 = "0123456789"
    //console.log("booking", booking)
    // const prova = 'QmAwesomeIpfsHash'
    const hashClient = await advisor.dbBookings.put({ _id: randomCode, idClient, idAdvisor, selectedDateBooked, randomCode })
    const hashClient1 = await advisor.dbBookings.put({ _id: prova, idClient: idAdvisor,idAdvisor: idClient , selectedDateBooked, prova })
    const hashClient2 = await advisor.dbBookings.put({ _id: prova2,idClient: idAdvisor,idAdvisor: idClient, selectedDateBooked, prova2 })
    //const hashAdvisor = await advisor.dbBookings.put(idAdvisor, data)
   // console.log("hashClient",hashClient)
    const entryClient = await advisor.dbBookings.get(randomCode)
    const all = await advisor.dbBookings.query((doc) => doc.idClient == idClient)
    const all2 = await advisor.dbBookings.query((doc) => doc.idAdvisor == idClient)

    //const entryAdvisor = await advisor.dbBookings.get(idAdvisor)
   console.log("entryClient", entryClient)
    console.log("all idClient", all)
    console.log("all idAdvisor", all2)
      console.log("randomCode",randomCode)

  }
  
  
  const handleOnOrbitPut = async (account,advisorAccount) => {
  
       
    const hash = await this.state.db.put(account,advisorAccount);
    //await state.db.close()
    console.log("hash",hash)
    console.log("this.props.myAccount en App.js handleOnOrbitPut", account)
    const entry = await this.state.db.get(account)
    // console.log("entry", entry)
    // console.log("entry.name", entry.name)
   // this.handleOnLoadProfile(AccountId)

}
  return (
    <Fragment>
      <DateTimePicker
        label="DateTimePicker"
        inputVariant="filled"
        value={selectedDate}
        onChange={handleDateChange}
      />

       <Button 
            size="small" 
            color="primary" 
            variant="contained"
           onClick={ () => handleBooking(advisor,selectedDate)}
           >
              Schedule a Meeting
              </Button>
    </Fragment>
  );
}

export default withContext(Scheduler);