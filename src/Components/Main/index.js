import React, { Fragment } from "react"
import {
  Grid,
  Paper,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  CardActions,
  Button
  
} from "@material-ui/core"

import DeleteIcon from "@material-ui/icons/Delete"
import EditIcon from "@material-ui/icons/Edit"
import VisibilityIcon from '@material-ui/icons/Visibility';
import CallIcon from '@material-ui/icons/Call';

import Form from "./Form"
import { withContext } from "../../context"
import { withStyles } from '@material-ui/core/styles';
import Scheduler from './Scheduler'
import GenerateRandomCode from 'react-random-code-generator'

const styles = theme => ({
  paper: {
    padding: 30,
    marginTop: 5,
    //marginBottom: 10,
    height: 500,
    overflowY: "auto",
    color: 'primary'
  },
  new: {
    padding: 30,
    marginTop: 5,
    //marginBottom: 10,
    height: 500,
    overflowY: "auto",
    color: 'primary'
  },

})



const advisors = ({
  classes,
  advisor,
  advisorAccount,
  advisors,
  advisorsAccount,
  advisorsByCategory,
  advisorsOnline,
  categories,
  categorySelected,
  onSelect,
  advisor: { id, name, description, online },
  onDelete,
  onSelectEdit,
  editMode,
  onEdit,
  drizzleState,
  db,
  myAccount,
  onLoadProfile,
  onOpenVideoApp,
  onDeleteAccount,
  ethApi,
  onCallAdvisorSelect,
  etherPrice,
  onOpenScheduler,
  openScheduler,
  bookingsByUser,
  myBookingsClient,
  myBookingsAdvisor,
  onSetRoomId,
  defaultRoom,
  openVideoCall
}) => {

  //console.log("este aqui",bookingsByUser)
  return(
  <Grid container spacing={1} >
    <Grid item xs={12} sm={4}>
    
      <Paper className={classes.paper}>
        {advisorsOnline.map(([group, advisors]) =>
          !categorySelected || categorySelected === group ? (
            <Fragment key={group}>
              <Typography
                variant="h5"
                color='secondary'
                style={{textTransform: 'capitalize'}}
              >
               Online: {group}
              </Typography>

              <List component="ul" aria-label="secondary mailbox folders">
                {advisors.map(({ id, name, priceHour, categories }) => { 
                
                  const priceHourEth = Math.round(priceHour/etherPrice *1000)/1000
                  return (
                  <ListItem key={id} button onClick={() => onSelect(id)}>

        <Card >
      <CardActionArea>
   
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" style={{ textTransform: "capitalize" }}>
          
                      {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
                    
                    {categories}  Price/Hour:  {priceHour} $  {priceHourEth}  Ether

                    

                 </Typography>
               
                    </CardContent>
                    
                    </CardActionArea>
                           <CardActions>
                           
                           {/* <Button 
                           size="small" 
                           color="primary" 
                           variant="contained"
                           onClick={ () => onOpenScheduler()}>
                             Schedule a Meeting
                             </Button> */}
                        <IconButton color='secondary' onClick={() => onOpenVideoApp(openVideoCall,defaultRoom)}>
                          {/* //defaultRoom es la generica que sirve para el online generico, este hay que generar una room id como con schedule */}
                        <CallIcon/> 
                      </IconButton>
                    
                      <IconButton color='secondary' onClick={() => onSelectEdit(id)}>
                        <EditIcon />
                      </IconButton>
                      <IconButton color= 'secondary'
                       
                      onClick={() => onDelete(id)}>

                        <DeleteIcon />
                      </IconButton>         
                  </CardActions>
                    </Card>
                  
                  </ListItem>
                )})}
              </List>
            </Fragment>
          ) : null
        )}
      </Paper>
    </Grid>

    <Grid item xs={12} sm={4} >
      <Paper className={classes.paper}>
        {editMode ? (
          <Form 
          advisor={advisor} 
          categories={categories} 
          onSubmit={onEdit}
        />
        ) : (
          <Fragment>
            <Typography variant="h4" color='secondary' style={{ textTransform: "capitalize" }}>
              {name} 
            </Typography>
            <Typography variant="h6">{description}</Typography>
            {name ? 
            <Fragment>
              <Typography variant="h6">Pick a Date </Typography>
            <Scheduler advisor={advisor}/>
              </Fragment> : null}
          </Fragment>
        )}
      </Paper>
    </Grid>
    
   


    <Grid item xs={12} sm={4}>
      <Paper className={classes.paper}>

          <Fragment>
            <Typography variant="h4" style={{ textTransform: "capitalize" }}>
              My Meetings
            </Typography>
              <Typography>
             Next Meeting
                </Typography>
                {myBookingsClient ? (
                  
                  <Fragment>
                    <Typography
                    variant="h4"
                    color="secondary">
                   with My Advisors
                     </Typography>
               
                <List component="ul" aria-label="secondary mailbox folders">
                {myBookingsClient.map(({ _id, idClient, randomCode, idAdvisor, selectedDateBooked }) => { 
                      const dbAdvisor = db.get(idAdvisor)
       // lo ideal seria ordenarlos por fecha TODO
                  return (
                  <ListItem key={_id} button onClick={() => onSelect(idAdvisor)}>

        <Card >
      <CardActionArea>
   
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" style={{ textTransform: "capitalize" }}>
                        {/* {db.get(idAdvisor)} */}
                     With:  {dbAdvisor.name}  PriceHour: {dbAdvisor.priceHour} $
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
                    
                   RandomCode:  {randomCode} 


                 </Typography>

               
                    </CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                    
                    Date: {selectedDateBooked}  


                 </Typography>  
                    </CardActionArea>
                           <CardActions>
                        <IconButton 
                        color='secondary' 
                        onClick={() => onOpenVideoApp(openVideoCall,randomCode,idClient)} 
                        >
                        <CallIcon/> 
                      </IconButton>
                    
                      <IconButton color='secondary' onClick={() => onSelectEdit(idAdvisor)}>
                        <EditIcon />
                      </IconButton>
                      <IconButton color= 'secondary'
                       
                      onClick={() => onDelete(id)}>

                        <DeleteIcon />
                      </IconButton>         
                  </CardActions>
                    </Card>
                  
                  </ListItem>
                )})}
              </List>
              </Fragment>
                ) : null}
               {myBookingsAdvisor ? (
                  
                  <Fragment>
                    <Typography 
                    variant="h4"
                    color="secondary">
                    with My Clients
                     </Typography>
               
                <List component="ul" aria-label="secondary mailbox folders">
                {myBookingsAdvisor.map(({ _id, idClient, randomCode, idAdvisor, selectedDateBooked }) => { 
                      const dbClient = db.get(idClient)


                  return (
                  <ListItem key={_id} button onClick={() => onSelect(idClient)}>

        <Card >
      <CardActionArea>
   
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" style={{ textTransform: "capitalize" }}>
          
                     With:  {dbClient.name} 
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
                    
                   RandomCode:  {randomCode} 


                 </Typography>

               
                    </CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                    
                    Date: {selectedDateBooked}  


                 </Typography>  
                    </CardActionArea>
                           <CardActions>
                        <IconButton 
                        color='secondary'    //acordarse de cambiar _id por randomCode cuando se cambie lo de "prova"
                        onClick={() => onOpenVideoApp(openVideoCall,_id,idClient)} 
                        >
                        <CallIcon/> 
                      </IconButton>
                    
                      <IconButton color='secondary' onClick={() => onSelectEdit(idClient)}>
                        <EditIcon />
                      </IconButton>
                      <IconButton color= 'secondary'
                       
                      onClick={() => onDelete(id)}>

                        <DeleteIcon />
                      </IconButton>         
                  </CardActions>
                    </Card>
                  
                  </ListItem>
                )})}
              </List>
              </Fragment>
                ) : null}

            <Typography 
              variant="h6"
              >
           Click here
            
            </Typography>
            <IconButton onClick={() => onLoadProfile(myAccount)}>
                        <VisibilityIcon />
                      </IconButton>
                      <Button onClick={() => bookingsByUser()}>
                        GetAllBookings
                      </Button>
          </Fragment>
      </Paper>
    </Grid>

   
          
    
  </Grid>
  )
}

export default withContext(withStyles(styles)(advisors))








  


    
  

