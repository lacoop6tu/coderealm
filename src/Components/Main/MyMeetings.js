
import React, { Fragment, useState } from "react";
import { DateTimePicker } from "@material-ui/pickers";
import { withContext } from "../../context"
import {
  // Grid,
  // Paper,
  // Typography,
  // List,
  // ListItem,
  // ListItemText,
  // ListItemSecondaryAction,
  // IconButton,
  // Card,
  // CardActionArea,
  // CardContent,
  // CardMedia,
  // CardActions,
  Button
  
} from "@material-ui/core"
import GenerateRandomCode from 'react-random-code-generator'



const Scheduler = (
  advisor,
  advisorAccount,
  myAccount,
  dbBookings
  ) => {

  console.log("advisorAccount",advisor.myAccount)
  const [selectedDate, handleDateChange] = useState(new Date());


  const handleBooking = async (advisor,selectedDate) => {
    const randomCode = GenerateRandomCode.TextNumCode(6, 6);    
      const idClient = advisor.myAccount
        const idAdvisor = advisor.advisor.id
        const selectedDateBooked = selectedDate.toString()
        const data = {idClient, idAdvisor, selectedDateBooked, randomCode}
    const booking = { idClient, idAdvisor, selectedDateBooked, randomCode}
    //console.log("booking", booking)
    // const prova = 'QmAwesomeIpfsHash'
    const hashClient = await advisor.dbBookings.put({ _id: randomCode, idClient, idAdvisor, selectedDateBooked, randomCode })
    //const hashAdvisor = await advisor.dbBookings.put(idAdvisor, data)
    console.log("hashClient",hashClient)
    const entryClient = await advisor.dbBookings.get('')
    //const entryAdvisor = await advisor.dbBookings.get(idAdvisor)
   console.log("entryClient", entryClient)
    //console.log("entryAdvisor", entryAdvisor)
    
      console.log("randomCode",randomCode)

  }
  
  
  const handleOnOrbitPut = async (account,advisorAccount) => {
  
       
    const hash = await this.state.db.put(account,advisorAccount);
    //await state.db.close()
    console.log("hash",hash)
    console.log("this.props.myAccount en App.js handleOnOrbitPut", account)
    const entry = await this.state.db.get(account)
    // console.log("entry", entry)
    // console.log("entry.name", entry.name)
   // this.handleOnLoadProfile(AccountId)

}
  return (
    <Fragment>
    <Typography variant="h4" style={{ textTransform: "capitalize" }}>
      My Meetings
    </Typography>
      <Typography>
     Next Meeting
        </Typography>
        {myBookingsClient ? (
          <Fragment>

       
        <List component="ul" aria-label="secondary mailbox folders">
        {myBookingsClient.map(({ idAdvisor, randomCode }) => { 
          
          const priceHourEth = Math.round(priceHour/etherPrice *1000)/1000
          return (
          <ListItem key={randomCode} button onClick={() => onSelect(id)}>

            < Card color="secondary">
            <CardActionArea>

          <CardContent>
           <Typography gutterBottom variant="h5" component="h2" style={{ textTransform: "capitalize" }}>
  
              {name}
         </Typography>
      <Typography variant="body2" color="textSecondary" component="p">
            
            {categories}  Price/Hour:  {priceHour} $  {priceHourEth}  Ether


         </Typography>
       
            </CardContent>
            
            </CardActionArea>
                   <CardActions>
                   
                   {/* <Button 
                   size="small" 
                   color="primary" 
                   variant="contained"
                   onClick={ () => onOpenScheduler()}>
                     Schedule a Meeting
                     </Button> */}
                <IconButton color='secondary' onClick={() => onOpenVideoApp()}>
                <CallIcon/> 
              </IconButton>
            
              <IconButton color='secondary' onClick={() => onSelectEdit(id)}>
                <EditIcon />
              </IconButton>
              <IconButton color= 'secondary'
               
              onClick={() => onDelete(id)}>

                <DeleteIcon />
              </IconButton>         
          </CardActions>
            </Card>
          
          </ListItem>
        )})}
      </List>
      </Fragment>
        ) : null}
    <Typography 
      variant="h6"
      >
            Click here
    
    </Typography>
     <IconButton onClick={() => onLoadProfile(myAccount)}>
                <VisibilityIcon />
              </IconButton>
              <Button onClick={() => bookingsByUser()}>
                GetAllBookings
              </Button>
         </Fragment>
  );
}

export default withContext(Scheduler);