import React, { Fragment, Component } from "react"
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  withStyles
} from "@material-ui/core/"
import { Add } from "@material-ui/icons"
import Form from "./Form"
import { withContext } from "../../context"

class CreateDialog extends Component {
  state = {
    open: false,
    
  }

  

  handleToggle = () => {
    this.setState({
      open: !this.state.open
    })
  }

  // handleOnOrbitPut = advisor => {
  //   this.onOrbitPut(advisor)
  // }

  handleFormSubmit = (id, profile, advisor) => {
    this.handleToggle()
    // const account = this.props.myAccount
    // const profile = this.props.advisorAccount
    const { myAccount, advisorAccount } = this.props
    this.props.onCreateProfile(advisorAccount)
    this.props.onCreate(advisorAccount)
    this.props.onOrbitPut(myAccount,advisorAccount)
    console.log(" dentro de dialog", myAccount)
    console.log(" dentro de dialog", advisorAccount)
    
  }

  render() {
    const { open } = this.state
    const { categories } = this.props
   console.log("en dialog render",this.props.myAccount)
    //const { onSubmit } = this.props

    return (
      <Fragment>
        <Button
          variant="outlined"
          //variant="fab"  funciona mal, hay que actualizarlo simplemente
          //variant="raised"
          onClick={this.handleToggle}
          color="secondary"
        >
         New   
          <Add />
        </Button>
        <Dialog open={open} onClose={this.handleToggle}>
          <DialogTitle id="form-dialog-title">Create a new profile</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please fill out the form below.
            </DialogContentText>
            <Form categories={categories} onSubmit={this.handleFormSubmit} />
          </DialogContent>
        </Dialog>
      </Fragment>
    )
  }
}

export default withContext(CreateDialog)
