import React, { Component, Fragment } from "react"
import { DrizzleContext } from '@drizzle/react-plugin'
import {
  Button,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  withStyles
} from "@material-ui/core/"
import { withContext } from "../../context"

const styles = theme => console.log(theme) || ({
  FormControl: {
    width: 400
  }
})


  class Form extends Component {
    
    

    state = this.getInitState()

    
    


    getInitState() {
      const { advisorAccount  } = this.props
        // this.setState(drizzleState)
      const empty = {  
          name: "",
          description: "",
          categories: "" ,
          priceHour: ""
        } 
      
      return (
            advisorAccount || empty    
        )
      }
    
    // componentWillReceiveProps( {advisor}) {
    //   console.log('fired')
    //   this.setState({                        probablememente ya no sirve cuando usas Context
    //     advisor
    //   })
    // }

    handleChange = ({ target: { value, name } }) =>
      this.setState({
        [name]: value
      })

    handleChangePriceHour= ({target: {value, priceHour}})=> {
      this.setState({
        [priceHour]: value
      })
    }

    handleSubmit = (myAccount, advisorAccount, advisor) => {
      
//.replace(/ /g, "-"),
      this.props.onSubmit(myAccount, advisorAccount, advisor)
       
    
    }

    // { classes, categories: fields } = this.props

    render() {
      //  { classes, categories:fields } = this.props
      const { id, name, description, categories, priceHour } = this.state
      console.log("STATE IN RENDER FORM",this.state)
      //console.log(this.props)
      const { classes, categories: fields, advisorAccount } = this.props
      
      return (
        
          <Fragment>
          <form>
          <TextField
            //required
            label="Name"
            value={name}
            name="name"
            onChange={this.handleChange}
            margin="normal"
            className={classes.FormControl}
          />
          <br />
          <FormControl className={classes.FormControl}>
            <InputLabel id="categories">Categories</InputLabel>
            <Select
              labelId="categories-label"
              id="categories-select"
              value={categories}
              onChange={this.handleChange}
              name="categories"
              //className={classes.FormControl}
            >
              {fields.map(field => (
                <MenuItem key={field} value={field}>
                  {field}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <br />
          <TextField
            //required
            label="PriceHour in dollars"
            value={priceHour}
            name="priceHour"
            onChange={this.handleChangePriceHour}
            margin="normal"
            className={classes.FormControl}
          />
          <br/>
          <TextField
            //required
            multiline
            rows="4"
            label="Description"
            name="description"
            value={description}
            onChange={this.handleChange}
            margin="normal"
            className={classes.FormControl}
          />
          <br />
          <Button
            color="secondary"
            onClick={this.handleSubmit}
            disabled={!name || !categories}
          >
            {advisorAccount ? "Edit" : "Create"}
          </Button>
        </form>    
        </Fragment>    
         )
       


    }
  }


export default withContext(withStyles(styles)(Form))