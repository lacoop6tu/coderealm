import Token from '../artifacts/Token.json'
import Platform from '../artifacts/Platform.json'

const drizzleOptions = {
    contracts: [Token,Platform],
  events: {
    Token : ["Transfer"],
  },

  web3: {
    fallback: {
      type:"ws",
      url:"ws://127.0.0.1:8545"
    }
  }
}


export default drizzleOptions;