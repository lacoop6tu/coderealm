import React, { useState, useEffect, useRef, Fragment } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles'
import ImageUploader from "react-images-upload"
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Checkbox from '@material-ui/core/Checkbox'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
// import Button from '@material-ui/core/Button'
// import SaveIcon from '@material-ui/icons/Save'
import Fab from '@material-ui/core/Fab'
import TrendingFlat from '@material-ui/icons/TrendingFlat'
import AddIcon from '@material-ui/icons/Add'
import Snackbar from '@material-ui/core/Snackbar'
import CloseIcon from '@material-ui/icons/Close'
// import IconButton from '@material-ui/core/IconButton'
import Gallery from 'react-grid-gallery'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone } from '@fortawesome/free-solid-svg-icons'
import { faVideo } from '@fortawesome/free-solid-svg-icons'
import _ from 'lodash'
import { useDispatch, useSelector } from 'react-redux'
import Sidebar from "react-sidebar"
// import Button from '@material-ui/core/Button'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import axios from 'axios'
import Divider from '@material-ui/core/Divider'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
// import List from '@material-ui/core/List'
// import ListItem from '@material-ui/core/ListItem'
// import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import VideocamIcon from '@material-ui/icons/Videocam';
import CallIcon from '@material-ui/icons/Call'
import ImageIcon from '@material-ui/icons/Image'
import WorkIcon from '@material-ui/icons/Work'
import BeachAccessIcon from '@material-ui/icons/BeachAccess'
// import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'

import { server_url, enter_path, getonlineusers_path, createroom_path, enterroom_path, countries, languages } from './constants'
import {
    // addUserToOnline,
    setOnlineUsers,
    login,
    activeLocalSrc,
    activePeerSrc,
    initLocalSrc,
    initPeerSrc,
    incomingCall,
    calling,
    establishedCall,
    endCall,
    rejectCall,
    enterRoom,
    exitRoom
} from './redux/actions'

import PeerConnection from './PeerConnection'
import CallWindow from './CallWindow'
import CallModal from './CallModal'
import socket from './socket'
import './App.scss'

import { withContext } from "../../context"
import {tokens, ether, formatBalance, toWei, ETHER_ADDRESS} from "../../helpers"

import {
    Grid,
    Paper,
    Typography,
    List,
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
    IconButton,
    Card,
    CardActionArea,
    CardContent,
    CardMedia,
    CardActions,
    Button
    
  } from "@material-ui/core"
  

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />
const checkedIcon = <CheckBoxIcon fontSize="small" />

const useStyles = makeStyles(theme => ({
    app: {
        flex: 1,
        marginTop: 100,
        color: 'black'
    },
    login: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    txtClientName: {
        color: 'black',
        marginLeft: 20,
        fontSize: 15,
        height: 40,
        border: 'none',
        borderBottomColor: 'black',
        borderBottom: 'solid 1px',
        backgroundColor: 'white',
    },
    imageUploader: {
        width: '30%',
        backgroundColor: 'transparent',
    },
    option: {
        fontSize: 15,
        '& > span': {
            marginRight: 10,
            fontSize: 18,
            color: 'black',
        },
    },
    country: {
        width: 300,
        color: 'black',
        marginTop: theme.spacing(2),
    },
    countryText: {
        root: {
            '& input:valid + fieldset': {
                borderColor: 'black',
                borderWidth: 2,
            },
        }
    },
    fab: {
        margin: theme.spacing(1),
    },
    close: {
        padding: theme.spacing(0.5),
    },
    chatRoom: {
        width: '80%',
        height: 800,
        overflow: 'auto',
        overflowX: 'hidden',
        border: "1px solid #ddd",
    },
    captionStyle: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: "rgba(0, 0, 0, 0.8)",
        height: 50,
        overflow: "hidden",
        position: "absolute",
        bottom: "0",
        width: "100%",
        color: "white",
        padding: "2px",
        fontSize: "90%"    
    },
    callButton: {
        pointerEvents: 'auto',
        cursor: 'pointer',
        color: 'green'
    },
    sidebar: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        overflowY: 'auto',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
    },
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        width: 400,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    rooms: {
        // flex: 1,
        flexDirection: 'column',
        backgroundColor: '#4791db60'
    }
}));

const CssTextField = withStyles({
    root: {
        '& input': {
            color: 'black',
        },
        '& button': {
            color: 'black',
        },
        '& label': {
            color: 'black',
        },
        '& label.Mui-focused': {
            color: 'black',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'black',
                color: 'black'
            },
            // '&:hover fieldset': {
            //   borderColor: 'white'
            // },
            '&.Mui-focused fieldset': {
                borderColor: 'black'
            },
        },
    },
})(TextField);

function countryToFlag(isoCode) {
    return typeof String.fromCodePoint !== 'undefined'
        ? isoCode.toUpperCase().replace(/./g, char => String.fromCodePoint(char.charCodeAt(0) + 127397))
        : isoCode;
}

const App = ({
    myAccount, 
    advisorAccount, 
    onOpenVideoApp, 
    advisor, 
    ethApi, 
    drizzle,
    drizzleState,
    onStartCallEth,
    onEndCallEth,
    etherPrice,
    roomCode,
    idClient
    }) => {
    const classes = useStyles()
    const [blogin, setBlogin] = useState(false)
    const [postImage, setPostImage] = useState(null)
    const [snack, setSnack] = useState({ open: false, message: '' })
    //const [country, setCountry] = useState(null)
    //const [language, setLanguage] = useState([])
    const userID = useRef('')
    //const [clientName, setClientName] = useState('')
    const [callModal, setCallModal] = useState({ callModal: '', callFrom: '' })
    const [callWindow, setCallWindow] = useState('')
    const [localSrc, setLocalSrc] = useState(null)
    const [peerSrc, setPeerSrc] = useState(null)
    const [selectClient, setSelectClient] = useState(null)
    const [openRoomModal, setopenRoomModal] = useState(false)
    const [roomName, setroomName] = useState(roomCode)
    const [currentRoom, setcurrentRoom] = useState(null)
    const onlineUsers = useSelector(state=>state.onlineUsers)
    const rooms = useSelector(state=>state.Rooms)

    
    
    const dispatch = useDispatch()
    const pc = useRef({})
    const conf = useRef(null)
    
    //const priceEth = etherPrice
    const advisorPriceEth = toWei(advisor.priceHour/etherPrice/60)
    //console.log("ethAPi", ethApi )
    // console.log("advisorPriceeth",advisor.priceHour/etherPrice/60)
    //console.log("const", Math.floor(advisorPriceEth))
    const priceWeiMinute = Math.floor(advisorPriceEth)
   
    //console.log("drizzle in webrtc",drizzle)

    const startCallIF = () => drizzle.contracts.Platform.methods.startCall(advisor.id, priceWeiMinute, 600).send({from: myAccount , gas:3000000})

    const handleStartCall = () => {
     
            startCallIF()
}

    const startCallHandler = (isCaller, friendID, config)=>{
        
        if (idClient === myAccount) {handleStartCall()}

        conf.current = config;
        pc.current = new PeerConnection(friendID)
          .on('localStream', (src) => {
            setCallWindow('active')
            dispatch(calling(friendID))
            setLocalSrc(src)
            dispatch(activeLocalSrc())
            if (!isCaller) {
                setCallModal({
                    callModal: '',
                    callFrom: ''
                })
            }
          })
          .on('peerStream', src => {
            dispatch(activePeerSrc())
            setPeerSrc(src)
          })
          .start(isCaller, config);
      }
    
    const endCallHandler = isStarter => {
        if (!isStarter) {
            dispatch(rejectCall())
        } else {
            dispatch(endCall())
        }
        if (_.isFunction(pc.current.stop)) {
            pc.current.stop(isStarter);
        }
        pc.current = {};
        conf.current = null;
        setCallWindow('')
        setCallModal({callModal:'', callFrom: ''})
        setLocalSrc(null)
        dispatch(initLocalSrc())
        setPeerSrc(null)
        dispatch(initPeerSrc())
    }

    const rejectCallHandler = ()=>{
        socket.emit('end', { to: callModal.callFrom })
        setCallModal({
            callModal: '',
            callFrom: ''
        })
    }
    
    const getonlineusers = () => {
        axios.post(server_url + '/' + getonlineusers_path, {
            clientId: userID.current
            
        })
        .then((res) => {
            const onlineUsers = res.data.onlineUsers
            const onlineRooms = res.data.onlineRooms
            dispatch(setOnlineUsers(onlineUsers, onlineRooms))
            
        }).catch((error) => {
            console.log(error.message)
        });

    }


    useEffect(() => {
        if (blogin && advisorAccount) {
            const intervalID = setInterval(()=>{
                getonlineusers()
            }, 10000)
            return () => {
                clearInterval(intervalID)
            }
        }
    }, [blogin])


    const enterVideoCall = () => {
       setopenRoomModal(true)
        
        const userData = advisorAccount
        console.log("consta" ,userData.id)
        console.log("adAcc" ,advisorAccount)
        const userDataId = userData.id
        const userDataName= userData.name
        const userDataCategory= userData.categories
        
        // if (!clientName) {
        //     setSnack({ open: true, message: 'Please enter your name!' })
        //     return

        socket.on('init', ({ id: cid }) => {
            document.title = `${userData.name} - VideoCall`
            userID.current = cid
            console.log('userID.current',userID.current)
            const formData = new FormData()
            //formData.append('postImage', postImage)
            formData.set('clientId', userID.current)
            formData.set('clientName',userDataName)
            formData.set('country', userDataId)
            formData.set('language', userDataCategory)
            axios.post(server_url + '/' + enter_path, formData, {headers: {
                'content-type': 'multipart/form-data'
            }}).then((res) => {
                console.log("res de app.js",res)
                dispatch(login(userDataName, postImage, userDataId, userDataCategory))
                setBlogin(true)
            }).catch((error) => {
                console.log(error.message)
            });
        }).on('request', ({ from: callFrom }) => {
            setCallModal({ callModal: 'active', callFrom })
            dispatch(incomingCall(callFrom))
        }).on('call', (data) => {
            if (data.sdp) {
                pc.current.setRemoteDescription(data.sdp);
                if (data.sdp.type === 'offer') {
                    pc.current.createAnswer()
                }
            } else {
                pc.current.addIceCandidate(data.candidate)
            }
        }).on('end', ()=>endCallHandler(false)).emit('init');



        // handleCreateRoom()

        // if (roomName) {
        //     axios.post(server_url + '/' + createroom_path, {
        //         clientId: userID.current,
        //         roomName
        //     }).then((res) => {
        //         // const onlineUsers = res.data.onlineUsers
        //         // const rooms = res.data.rooms
        //         // dispatch(setOnlineUsers({onlineUsers, rooms}))
        //     }).catch((error) => {
        //         console.log(error.message)
        //     });
        // } else {
        //     setSnack({ open: true, message: 'Please enter room name!' })
        //     return
        // }
    }

    const handleSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnack(false);
    }

    const handlePostingImage = (pictureFiles, pictureDataURLs) => {
        setPostImage(pictureFiles[0])
    }


    const onCurrentImageChange = (index)=>{
        setSelectClient(index)
    }
    
    
    
    
    const ChattingRoom = ({onlineUsers,drizzle, drizzleState}) => {
               
        return (
            <React.Fragment >
            <Grid container spacing={1} >
                     <Grid item >
                <Paper className={classes.paper}>
       
          
              <Typography
                variant="h5"
                color='secondary'
                style={{textTransform: 'capitalize'}}
              >
               USERS ONLINE
              </Typography>
            
              <List component="ul" aria-label="secondary mailbox folders">
                {onlineUsers.map(({ clientId, clientName, language }) => (
                  <ListItem key={clientId} >
                    <ListItemText
                      style={{ textTransform: "capitalize" }}
                      primary={clientName}
                      secondary={language}
                      />
                      
                  <ListItemSecondaryAction>
                    <IconButton 
                    color='primary' 
                    onClick={
                    ()=>{
                        if (clientId == userID.current) {
                            return
                        }

                    
                        startCallHandler(true, clientId, {audio: true, video: true})
                        
                    }
                    }>
                        <VideocamIcon/>
                      </IconButton>
                    
                      <IconButton 
                            color='primary' 
                            onClick={()=>{
                        if (clientId == userID.current) {
                            return
                        }
                        startCallHandler(true, clientId, {audio: true, video: false})
                    }}>
                        <CallIcon/>
                      </IconButton>
                    
                    </ListItemSecondaryAction>
                    
                
                  </ListItem>
                ))}
              </List>
        
          
                        </Paper>
                    </Grid>
                    </Grid >
            
            </React.Fragment>
        )
    }

    const RoomList = ({currentRoom, activeRoom, rooms, enterRoom}) => {
        return (
            <List>
              {/* <ListItem button selected={currentRoom?false:true}
                onClick={()=>activeRoom(null)}
                key='allusers'
              >
                <ListItemText primary="All users"/>
              </ListItem> */}
              {
                  rooms.map(item=>{
                      return (
                        <ListItem button
                            onClick={()=>activeRoom(item)}
                            selected={currentRoom&&currentRoom.id==item.id?true:false}
                            key={item.id}
                        >
                            {/* <ListItemAvatar>
                                <Avatar src={server_url+'/'+item.postImage.src}/>
                            </ListItemAvatar> */}
                            <ListItemText primary={item.roomName} id={item.id}/>
                            <ListItemSecondaryAction>
                            <Checkbox
                                edge="end"
                                onChange={e=>enterRoom(item, e.target.checked)}
                                checked={item.clientId==userID.current||item.users.includes(userID.current)?true:false}
                                inputProps={{ 'aria-labelledby': item.id }}
                            />
                            </ListItemSecondaryAction>
                        </ListItem>
                        )
                  })
              }
            </List>
          )
    }
  

    const handleChangeRoomName = e => {
        //setroomName(e.target.value)
    }
    const handleCreateRoom = () => {
        setopenRoomModal(false)
        if (roomName) {
            axios.post(server_url + '/' + createroom_path, {
                clientId: userID.current,
                roomName
            }).then((res) => {
                // const onlineUsers = res.data.onlineUsers
                // const rooms = res.data.rooms
                // dispatch(setOnlineUsers({onlineUsers, rooms}))
            }).catch((error) => {
                console.log(error.message)
            });
            //setcurrentRoom(roomName)
        } else {
            setSnack({ open: true, message: 'Please enter room name!' })
            return
        }
    }

    const handleActiveRoom = item => {
        if (item) {
            setcurrentRoom(item)
        } else {
            setcurrentRoom(null)
        }
    }

    const roomUsers = currentRoom?(onlineUsers.filter(item=>{
        if (item.clientId == userID.current) {
            return true
        }
        if (item.clientId == currentRoom.clientId) {
            return true
        }
        // if (currentRoom.users.includes(item.clientId)) {
        //     return true
        // }
        return false
    })):onlineUsers

    const handleEnterRoom = (room, checked)=>{
        setopenRoomModal(false)
        axios.post(server_url + '/' + enterroom_path, {
            clientId: userID.current,
            id: room.id,
            checked
        }).then((res) => {
            // const onlineUsers = res.data.onlineUsers
            // const rooms = res.data.rooms
            // dispatch(setOnlineUsers({onlineUsers, rooms}))
            if (checked) {
                dispatch(enterRoom(room.roomName))
            } else {
                dispatch(exitRoom(room.roomName))
            }
            getonlineusers()
        }).catch((error) => {
            console.log(error.message)
        });
    }
   

    return (
        <Sidebar
            sidebar={
                <div className={classes.sidebar}>
                    <span className="navbar-brand">
                        <img src={require('./icon1.png')} /> TUSI Marketplace
                    </span>
                    <Button
                            variant="contained"
                            color="secondary"
                            //startIcon={<AddIcon />}
                            onClick={onOpenVideoApp}
                        >
                            Close
                        </Button>
{/* 
                    <Divider/>
                    {
                        blogin &&
                        <Button
                            variant="contained"
                            color="secondary"
                            startIcon={<AddIcon />}
                            onClick={()=>setopenRoomModal(true)}
                        >
                            Create room
                        </Button>
                    } */}
                    <Divider/>
                    {
                        blogin &&
                        <div className={classes.rooms}>
                            <RoomList
                                currentRoom={currentRoom}
                                activeRoom={handleActiveRoom}
                                rooms={rooms}
                                enterRoom={handleEnterRoom}
                            />
                        </div>
                    }
                </div>
            }
            docked={true}
        >
        <div className={classes.app}>
            {
                blogin
                    ?
                    <div className={classes.chatRoom}>
                        <ChattingRoom onlineUsers={roomUsers}/>
                        {conf.current &&
                        <CallWindow
                            status={callWindow}
                            localSrc={localSrc}
                            peerSrc={peerSrc}
                            config={conf.current}
                            mediaDevice={pc.current.mediaDevice}
                            endCall={endCallHandler}
                            drizzle= {drizzle}
                            idClient={idClient}
                            myAccount={myAccount}
                        />
                        }
                        <CallModal
                            status={callModal.callModal}
                            startCall={startCallHandler}
                            rejectCall={rejectCallHandler}
                            callFrom={callModal.callFrom}
                        />
                    </div>
                    :
                    <div className={classes.login}>
        
                       {idClient === myAccount ?       
                        (<Fragment>
                        <Typography> 
                           Here you will see details about the call and info needed before starting the call 
                           
                        </Typography>
                        <Typography>
                            Advisor you are going to call: {advisor.id}
                        </Typography>
                        <Typography> 
                           
                           Price Hour of your call in dollars: {advisor.priceHour} $
                           
                        </Typography>
                        <Typography> 
                           
                           Price Per minute in Ether: {ether(priceWeiMinute)} 
                           
                        </Typography>
                        <Typography> 
                           
                           Your ROOM ID: {roomCode} 
                           
                        </Typography>
                        </Fragment>)
                    : (<Typography> 
                    YOU ARE THE ADVISOR AND WILL EARN ETHER 
                    
                 </Typography>)      }


                        <Fab color="secondary" aria-label="edit" className={classes.fab}>
                            <TrendingFlat onClick={enterVideoCall} />
                        </Fab>
                    </div>

            }
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snack.open}
                autoHideDuration={2000}
                onClose={handleSnack}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{snack.message}</span>}
                action={[
                    // <Button key="undo" color="secondary" size="small" onClick={handleSnack}>
                    //   UNDO
                    // </Button>,
                    <IconButton
                        key="close"
                        aria-label="close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleSnack}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />
            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                open={openRoomModal}
                className={classes.modal}
                onClose={()=>setopenRoomModal(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <div className={classes.paper}>
               
                    {idClient === myAccount ? (   <Fragment>
                         <Button
                        variant="contained"
                        color="secondary"        
                    >
                        Check to enter Call with {advisor.name}
                    </Button>
                   
                     
                    
                    <List>
            
              {
                  rooms.map(item=>{
                      return (
                        <ListItem button
                            onClick={()=>handleActiveRoom(item)}
                            selected={currentRoom&&currentRoom.id==item.id?true:false}
                            key={item.id}
                        >
                            
                            <ListItemText primary={item.roomName} id={item.id}/>
                            <ListItemSecondaryAction>
                            <Checkbox
                                edge="end"
                                onChange={e=>handleEnterRoom(item, e.target.checked)}
                                checked={item.clientId==userID.current||item.users.includes(userID.current)?true:false}
                                inputProps={{ 'aria-labelledby': item.id }}
                            />
                            </ListItemSecondaryAction>
                        </ListItem>
                        )
                  })
              }
            </List>
                      

                   
                    </Fragment>
                    
                    ) 
                    : 
                    ( <Button
                        variant="contained"
                        color="secondary"
                        onClick={handleCreateRoom}
                    >
                        Confirm Enter Call with {advisor.name}
                    </Button>
                        

                    )}
                </div>
            </Modal>
            </div>
        </Sidebar>
    );
}

export default withContext(App);

