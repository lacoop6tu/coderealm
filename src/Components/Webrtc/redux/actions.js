
export const ADD_USER_TO_ONLINE = 'addUserToOnline'
export const SET_ONLINE_USERS = 'setOnlineUsers'
export const LOGIN = 'login'
export const ACTIVE_LOCAL_SRC = 'activeLocalSrc'
export const ACTIVE_PEER_SRC = 'activePeerSrc'
export const INIT_LOCAL_SRC = 'initLocalSrc'
export const INIT_PEER_SRC = 'initPeerSrc'
export const INCOMING_CALL = 'incomingCall'
export const CALLING = 'calling'
export const ESTABLISH_CALL = 'establishedCall'
export const END_CALL = 'endCall'
export const REJECT_CALL = 'rejectCall'
export const ENTER_ROOM = 'enterRoom'
export const EXIT_ROOM = 'exitRoom'

export const addUserToOnline = (onlineClient)=>{
    return {
        type: ADD_USER_TO_ONLINE,
        payload: onlineClient
    }
}

export const setOnlineUsers = (onlineUsers, onlineRooms)=>{
    return {
        type: SET_ONLINE_USERS,
        payload: {onlineUsers, onlineRooms}
    }
}

export const login = (clientName, postImage, country, language)=>{
    return {
        type: LOGIN,
        payload: {
            clientName, postImage, country, language
        }
    }
}

export const activeLocalSrc = ()=>{
    return {
        type: ACTIVE_LOCAL_SRC
    }
}

export const activePeerSrc = ()=>{
    return {
        type: ACTIVE_PEER_SRC
    }
}

export const initLocalSrc = ()=>{
    return {
        type: INIT_LOCAL_SRC
    }
}

export const initPeerSrc = ()=>{
    return {
        type: INIT_PEER_SRC
    }
}

export const incomingCall = (callFrom)=>{
    return {
        type: INCOMING_CALL,
        payload: callFrom
    }
}

export const calling = (callTo)=>{
    return {
        type: CALLING,
        payload: callTo
    }
}

export const establishedCall = ()=>{
    return {
        type: ESTABLISH_CALL
    }
}

export const endCall = ()=>{
    return {
        type: END_CALL
    }
}

export const rejectCall = ()=>{
    return {
        type: REJECT_CALL
    }
}

export const enterRoom = roomName => {
    return {
        type: ENTER_ROOM,
        payload: roomName
    }
}

export const exitRoom = roomName => {
    return {
        type: EXIT_ROOM,
        payload: roomName
    }
}