import {SET_ONLINE_USERS} from '../actions'

export default (state=[], action) => {
    switch (action.type) {
        case SET_ONLINE_USERS:
            return action.payload.onlineRooms
        default:
            return state
    }
}