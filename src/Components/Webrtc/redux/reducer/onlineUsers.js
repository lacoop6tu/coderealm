import {ADD_USER_TO_ONLINE, SET_ONLINE_USERS} from '../actions'

export default (state=[], action) => {
    switch (action.type) {
        case ADD_USER_TO_ONLINE:
            const {clientId, clientName, country, language} = action.payload
            const onlineUsers = state
            onlineUsers.push({
                clientId, clientName, country, language
            })
            return onlineUsers
        case SET_ONLINE_USERS:
            return action.payload.onlineUsers
        default:
            return state
    }
}