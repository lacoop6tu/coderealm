import { combineReducers } from 'redux'
import onlineUsers from './onlineUsers'
import Rooms from './Rooms'

export default combineReducers({
    onlineUsers,
    Rooms
})
