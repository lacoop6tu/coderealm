import React, { Component, Fragment, useEffect, useState } from "react"
import CssBaseline from '@material-ui/core/CssBaseline'
import IpfsClient from 'ipfs-http-client';
import OrbitDB from 'orbit-db';
import { DrizzleContext } from '@drizzle/react-plugin'
import { Drizzle, generateStore } from "@drizzle/store";
import { Provider, FULL } from "../context"
import { toWei } from "../helpers"

import { categories, advisors, online } from "../store"


import { Header, Footer, NavPills,SearchBar,Prova } from "./Layouts"
import Main from "./Main"
import Scheduler from "./Main"
import AppWrapper from  "./Webrtc/AppWrapper"

import Token from '../artifacts/Token.json'
import Platform from '../artifacts/Platform.json'
import CoinGecko from 'coingecko-api'




const drizzleOptions = {
    contracts: [Token,Platform],
  events: {
    TokenTransfer : ["Transfer"],
    DepositEther : ["Deposit"]
  },

  web3: {
    fallback: {
      type:"ws",
      url:"ws://127.0.0.1:8545"
    },
  },
}


const drizzleStore = generateStore(drizzleOptions)
const drizzle = new Drizzle(drizzleOptions,drizzleStore)
const {initialized} = drizzle
const drizzleState = drizzleStore.getState()


export default class extends Component {
//   constructor(props) {
//   super(props);
//   this.state = {
//     value: {
//       advisors,
//       advisor: {},
//       ...this.state
//   }
// }
  state = {
    advisors,
    advisor: {},
  
    //editMode si no hay nada seria undefined que igualmente seria "false" en este caso especifico
    editMode: false,
    openVideoCall: false,
    openScheduler: false,
    defaultRoom: "onlineRoom",
    
  }


  getAdvisorsByCategory() {
    const initAdvisors = categories.reduce(
      (advisors, field) => ({
        ...advisors,
        [field]: []
      }),
      {}
    )
 

    //console.log(categories, initAdvisors)
    //Advisor es el objeto en store.js que corresponde a cada advisor
    return Object.entries(
      // const advisors = this.state.advisorsAccount
      advisors.reduce((advisors, advisor) => {
        const { categories } = advisor
        //CUANDO CREAMOS initAdvisors podemos evitar el operator "? :"
        advisors[categories] = [...advisors[categories], advisor]

        // CUANDO NO HAY CATEGORIAS CON EMPTY STRING
        // advisors[categories] = advisors[categories]
        //   ? [...advisors[categories], advisor]
        //   : [advisor]

        return advisors
      }, initAdvisors)
    )
  }

   getAllBookings = async (myAccount) => {
    const bookingsArrayClient = await this.state.dbBookings.get('')
    //hay que sacarlas con query doc

    // const bookingsArrayClient = await this.state.dbBookings.query((doc) => doc.idClient == myAccount)
    //const bookingsArrayAdvisor = await this.state.dbBookings.query((doc) => doc.idAdvisor = myAccount)
    console.log("bookingArray", bookingsArrayClient)

    const groupBy = (objectArray, property) => {
        return objectArray.reduce(function (total, obj) {
            let key = obj[property];
            if (!total[key]) {
                total[key] = [];
            }
            total[key].push(obj);

           
            //const prova = total.myAccount




            return total
        }, {});
    }
    
    const groupedByIdClient = groupBy(bookingsArrayClient, 'idClient');
    const myBookingsClient = groupedByIdClient[this.state.myAccount]
    const groupedByIdAdvisor = groupBy(bookingsArrayClient, 'idAdvisor')
    const myBookingsAdvisor = groupedByIdAdvisor[this.state.myAccount]

    this.setState({
      //groupedByIdClient: groupedByIdClient,
      myBookingsClient: myBookingsClient,
      myBookingsAdvisor: myBookingsAdvisor,
      groupedByIdAdvisor,
      groupedByIdClient
    })
    const prova = this.state.myAccount
    
    console.log("groupedByIdClient", this.state.groupedByIdClient)
    console.log("ale reservas de consulta", this.state.groupedByIdAdvisor["0x1bb60f730eA617C5a50b1dA07Fbe56F2A6AC6e4a"])
    console.log("myBookingsAdvisor", this.state.myBookingsAdvisor)
    console.log("myBookingsClient", this.state.myBookingsClient)
    //console.log("groupedByIdClient", this.state.groupedByIdClient[this.state.myAccount])
    
 
  }
  

  

   
    
    
    
  

  
  getAdvisorsOnline = () =>  {
    const initOnlineAdvisors= online.reduce(
      (advisors, online) => ({
        ...advisors,
        [online]: []
      }),
      {}
    )

    return Object.entries(
      //const advisorss = this.state.advisorsAccount
      advisors.reduce((advisors, advisor) => {
        const { online } = advisor
        //CUANDO CREAMOS initAdvisors podemos evitar el operator "? :"
        advisors[online] = [...advisors[online], advisor]

        // CUANDO NO HAY CATEGORIAS CON EMPTY STRING
        // advisors[categories] = advisors[categories]
        //   ? [...advisors[categories], advisor]
        //   : [advisor]

        return advisors
      }, initOnlineAdvisors)
    )
  }

  



  handleCategorySelected = categorySelected => {
    this.setState({
      categorySelected
    })
  }

  handleAdvisorSelected = (id) => {
    this.setState(({ advisors }) => ({
      advisor: advisors.find(ad => ad.id === id),
      
      editMode: false
    }))
    
  }

  handleAdvisorCreate = advisor => {
    this.setState(({ advisors }) => ({
      advisors: [...advisors, advisor]
    }))
  }

   handleAdvisorAccountCreate = advisorAccount => {
    this.setState(({ advisorsAccount }) => ({
      advisorsAccount: [...advisorsAccount, advisorAccount]
    }))
  }

  handleAdvisorDelete = id => {
    this.setState(
      ({ advisors, advisor, editMode,advisorsAccount, advisorAccount }) => ({
      advisors: advisors.filter(ad => ad.id !== id),
      editMode: advisor.id === id ? false : editMode,
      advisor: advisor.id === id ? {} : advisor,
      advisorsAccount: advisorsAccount.filter(ad => ad.id !== id),
      editMode: advisorAccount.id === id ? false : editMode,
      advisorAccount: advisorAccount.id === id ? {} : advisorAccount

    }))
    




      
  }

  handleAdvisorAccountDelete = id => {
    this.setState(({ advisorsAccount, advisorAccount, editMode }) => ({
      advisorsAccount: advisorsAccount.filter(ad => ad.id !== id),
      editMode: advisorAccount.id === id ? false : editMode,
      advisorAccount: advisorAccount.id === id ? {} : advisorAccount
    }))
  }
  handleAdvisorSelectEdit = id => {
    this.setState(({ advisors }) => ({
      advisor: advisors.find(ad => ad.id === id),
      editMode: true
    }))
  }

  handleAdvisorEdit = advisor => {
    this.setState(({ advisors }) => ({
      advisors: [...advisors.filter(ad => ad.id !== advisor.id), advisor],
      advisor,
      //editMode: true
    }))
    this.handleOnOrbitPut(advisor)
  }

  handleOnOrbitPut = async (account,advisorAccount) => {
  
       
      const hash = await this.state.db.put(account,advisorAccount);
      //await state.db.close()
      console.log("hash",hash)
      console.log("this.props.myAccount en App.js handleOnOrbitPut", account)
      const entry = await this.state.db.get(account)
      // console.log("entry", entry)
      // console.log("entry.name", entry.name)
     // this.handleOnLoadProfile(AccountId)

  }

 
   handleOnLoadProfile = async   (myAccount) => {
     
      const myLoadedProfile = await this.state.db.get(myAccount)
      const myMeetings = await this.state.dbBookings.get(myAccount)
      this.setState(({myBookings}) => ({
        myBookings: myBookings= myMeetings,
      }))
      console.log("myLoadedBookings", this.state.myBookings.selectedDateBooked)
      //console.log("stateeee", this.state.myLoadedBookings)
      //this.handleAdvisorAccountCreate(myLoadedProfile)
      //this.handleAdvisorCreate(myLoadedProfile)
      
  }

  handleSetOnline = () => {
    //const myProfile= this.state.
    //console.log("myProfile", myProfile)
    this.setState(({ advisorAccount }) => ({
      
      advisorAccount: advisorAccount.online = !advisorAccount.online,
      ...this.state
     
    }))
  }


  handleOpenVideoApp = (openVideoCall,Code,idClient) => {
    const roomCode = "casamia"
    this.setState({
      openVideoCall : openVideoCall = !openVideoCall,
      roomCode: roomCode,
      idClient: idClient = idClient
    })
  }

  handleOpenScheduler = (openScheduler) => {
    this.setState({
      openScheduler : openScheduler = !openScheduler
    })
  }
  // handleCallAdvisorSelected = id => {
  //   this.setState(({ advisors, myAdvisor }) => ({
  //     myAdvisor: myAdvisor = advisors.find(ad => ad.id === id)
  //     //editMode: false
  //   }))
  //   console.log("myAdvisor", this.state.myAdvisor)
  // }
   
   
  handleEndCallEth = () => {
    const callId = 1
   // drizzle.contracts.Platform.methods.withdrawEther(callId).send( {from: this.state.myAccount})
      
  }


  
  handleStartCallEth = () => {

    const priceEth = this.state.ethApi.data.ethereum.usd
  const advisorPriceEth = this.state.advisor.priceHour/priceEth/60


    // drizzle.contracts.Platform.methods.startCall(this.state.advisor.id, toWei(advisorPriceEth), 600).send({from: this.state.myAccount})
          
      //console.log("Start",Start)
  }


  handleSetRoomId = (randomCode) => {
    this.setState ({
      randomCode: randomCode
    })
  } 
  
 
  

   async componentDidMount () {


    
      const ipfs = IpfsClient('http://localhost:5001');
      const orbitdb = await OrbitDB.createInstance(ipfs);
      const db = await orbitdb.open('/orbitdb/zdpuAnWSnuMMB8HyFBU4FeXgnBwqdrozwLXT2dmKczeDRuw6t/first-database')
      const dbBookings = await orbitdb.docs('bookings')
      const advisorAccount = { id: "0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1", online: false, priceHour:"60", name:"Andrea", description:"I like smoking weed", categories:"Blockchain"}
      const { advisors } = this.state
      //console.log("advisors[0]",advisors[0])
      //await db.put(advisorAccount.id, advisorAccount)
      await db.put(advisors[0].id, advisors[0])
      await db.put(advisors[1].id, advisors[1])
      await db.put(advisors[2].id, advisors[2])
      await db.put(advisors[3].id, advisors[3])
      //console.log(db)
     
      const CoinGeckoClient = await new CoinGecko();
    
        
      //const data = await CoinGeckoClient.coins.fetch('ethereum', {})
      const ethApi = await CoinGeckoClient.simple.price({
        ids: ['ethereum'],
        vs_currencies: ['usd'],
    });
      // console.log("api",ethApi)
      // console.log("precio ethereym", ethApi.data.ethereum.usd)
          const etherPrice = ethApi.data.ethereum.usd 
      
      
        
        const advisorsAccount = [  
          //await db.get(advisorAccount.id),
          await db.get(advisors[0].id),
          await db.get(advisors[1].id),
          await db.get(advisors[2].id),
          await db.get(advisors[3].id)]
        
        
      const myAccount = advisorAccount.id
     
      
     

       this.setState({
        ipfs,
        db,
        dbBookings,
        orbitdb,
        ethApi,
        etherPrice,
        myAccount,
        advisorsAccount,
        advisorAccount,
        ...this.state
    
      })
       

     
      
      
    
  }


  

  getContext = ()=> ({
    categories,
    advisorsByCategory: this.getAdvisorsByCategory(),
    bookingsByUser: this.getAllBookings,
    advisorsOnline: this.getAdvisorsOnline(),
    ...this.state,
    onCategorySelect: this.handleCategorySelected,
    onCreate: this.handleAdvisorCreate,
    onEdit: this.handleAdvisorEdit,
    onSelectEdit: this.handleAdvisorSelectEdit,
    onDelete: this.handleAdvisorDelete,
    onSelect: this.handleAdvisorSelected,
    onOrbitPut: this.handleOnOrbitPut,
    onLoadProfile: this.handleOnLoadProfile,
    onCreateProfile: this.handleAdvisorAccountCreate,
    onDeleteAccount: this.handleAdvisorAccountDelete,
    onSetOnline: this.handleSetOnline,
    onOpenVideoApp: this.handleOpenVideoApp,
    onCallAdvisorSelect: this.handleCallAdvisorSelected,
    onStartCallEth: this.handleStartCallEth,
    onEndCallEth: this.handleEndCallEth,
    onOpenScheduler: this.handleOpenScheduler,
    onSetRoomId: this.handleSetRoomId,
    drizzle,
    drizzleState,
    drizzleStore
  })

  

  render() {
      
    
    console.log("PROPS",this.props)
    console.log("STATE",this.state)
    //console.log("myBookings", this.state.myBookings)
    // console.log("prueba precio", this.state.data)
    // console.log("DRIZZLE",drizzle)
    // console.log("DRIZZLE STORE", drizzleStore)

    //console.log("DRIZZLE STATE from drizzle",this.drizzleState)
    //console.log("getContext",this.getContext())

    return (
     
      
    <DrizzleContext.Provider drizzle={drizzle}>

     
        
      <DrizzleContext.Consumer>
         {drizzleContext => {
          const {drizzle, initialized, drizzleState} = drizzleContext;

          if(!initialized) {
            return "Loading..."
          }
      
          return( <Provider value={this.getContext()}     >
                  <CssBaseline />
                  
        { this.state.openVideoCall ?  <AppWrapper drizzle={drizzle} drizzleStore={drizzleStore} drizzleState={drizzleState}/>  : 
            <Fragment>
        {/* <Prova drizzle={drizzle} drizzleStore={drizzleStore} drizzleState={drizzleState} /> */}
        <Header drizzle={drizzle} drizzleStore={drizzleStore} drizzleState={drizzleState}   /> 
        <SearchBar/>
         {/* <Footer /> */}
         <Main/> 
        {/* <Scheduler/>  */}
        </Fragment>
        }
                  
                
                
                
                  

                  
              
               </Provider> )
        }}
     
     </DrizzleContext.Consumer>

     </DrizzleContext.Provider> 
    
       
       
    )
      }
}
