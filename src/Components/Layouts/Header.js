import React, {useState,useEffect, useContext, Fragment} from "react"
import { AppBar, Toolbar, Typography, Grid } from "@material-ui/core/"
import CreateDialog from "../Main/Dialog"
import { drizzleReactHooks, DrizzleContext } from '@drizzle/react-plugin'
import { newContextComponents } from "@drizzle/react-components"
import { FULL, withContext } from "../../context"
import EditIcon from "@material-ui/icons/Edit"
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import purple from '@material-ui/core/colors/purple'
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl'
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import {tokens, ether, formatBalance, toWei, ETHER_ADDRESS} from "../../helpers"
import Web3, { ethereum} from 'web3'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,

  },
  menuButton: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  
  
  
  
  
  
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
})); 

const { useDrizzle, useDrizzleState} = drizzleReactHooks
const { AccountData, ContractData, ContractForm } = newContextComponents


const Header = ({
  advisors,
  advisor,
  drizzle, 
  myAccount,
  drizzleState,
  onSetOnline,
  advisorAccount,
  ethApi,
  etherPrice,
 
}
)=> {
  
 

  const classes = useStyles();

  const [amountDeposit, setAmountDeposit] = React.useState();
 const [amountWithdraw, setAmountWithdraw] = React.useState();
 
       
 
 const depositEtherIF = (amount) => drizzle.contracts.Platform.methods.depositEther().send({ from: myAccount, value: toWei(amount)})
 const withdrawEtherIF = (amountW) => drizzle.contracts.Platform.methods.withdrawEther(amountW).send( {from: myAccount})
  

  const handleDeposit = () => {
    
      
   depositEtherIF(amountDeposit.value)
    
  }

  const handleWithdraw = () => {
    const amountWei = toWei(amountWithdraw.value)
      withdrawEtherIF(amountWei)
      
  }
  
  
  const handleChangeDeposit = ({target :{value, name}}) => {
    setAmountDeposit({ ...amountDeposit, value });
  };

  const handleChangeWithdraw = ({target :{value, name}}) => {
    setAmountWithdraw({ ...amountWithdraw, value });
  
  };
                   
 return( 
          <Fragment>   
        <AppBar 
            position="static"
            color="primary" >
          <Toolbar>
          <Grid container spacing={2} alignContent='center' alignItems='center' >
         <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton>
          
                <Grid item xs={12} sm={2} container alignContent='center' alignItems='center'>


                <Typography variant="button" >
                TUSI Marketplace </Typography>
                
                <FormControl component="fieldset">
                
                 <FormGroup aria-label="position" row>
              <FormControlLabel
                  value="top"
                control={<Switch 
                  color="secondary" 
                  size="small"  
                  name="checkedA" 
                  onClick= {onSetOnline}
                  />}
                  label="Online Status"
                  labelPlacement="top"
          
               />
       
        
        <FormControlLabel
          value="start"
          control={<Switch color="secondary" size="small"  name="checkedB"/>}
          label="Help newbies"
          labelPlacement="top"
        />
        
             </FormGroup>
             
    </FormControl>
    </Grid>
    


    
     <Grid item xs={12} sm={4}  container direction='column' alignContent='center' alignItems='center' > 

     <Typography variant="h6" align="center"  >
               My Wallet Address: 
                 
               </Typography>  
               <Typography variant="subtitle2" align="center"  >
              
                  {myAccount}
               </Typography> 
   
        
        <Typography variant="h5" color="secondary"> Your Available Balance @TUSI  </Typography>  
                 
        <Button variant="contained" color="secondary" size="large">
                <ContractData
        drizzle={drizzle}
        drizzleState={drizzleState}
        contract="Platform"
        method="balanceOf"
        methodArgs={[ETHER_ADDRESS,myAccount]}
        
        
        />
        &nbsp;&nbsp;&nbsp;
        Eth
        </Button>
    
       
        </Grid> 
        
        <Grid item xs={12} sm={4} >
         
             
               
               <Grid container  direction='row' alignItems='flex-start'>
                
               <Button 
               variant="contained" 
               size="small" 
               color="secondary" 
               className={classes.margin}
               onClick={handleDeposit}>
                Deposit
              </Button> 
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <TextField
         
          id="outlined-number1"
          name="depositAmount"
          label="Amount in Ether"
          type="number"
          color="secondary"
          size="small"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleChangeDeposit}
        />
            </Grid>
            <Grid container  direction='row' alignItems='flex-end'>
              <Button 
              variant="contained" 
              size="small" 
              color="secondary"
             className={classes.margin}
              onClick={handleWithdraw}
             >
                Withdraw 
              </Button>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <TextField
             
          id="outlined-number"
          label="Amount in Ether"
          name="withdrawAmount"
          type="number"
          color="secondary"
          size="small"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
          // value ={value}
          onChange={handleChangeWithdraw}
                />
                </Grid>
                </Grid>
              
             
                 <CreateDialog/>
                 </Grid>
                  </Toolbar>
                   </AppBar>
            
                   </Fragment>
      )
    
 			 
  					
  				 
          
          
          	
          	
}

export default withContext(Header);



