import Header from "./Header"
import Footer from "./Footer"
import NavPills from "./NavPills"
import SearchBar from "./SearchBar"
import Prova from "./Prova"

export { Header, Footer, NavPills, SearchBar, Prova}
