import React, { Component, Fragment } from "react"
import { AppBar, Typography, Tabs, Tab } from "@material-ui/core/"
import { withContext } from "../../context"

class Footer extends Component {
  
  onIndexSelect = (e, index) => {
    const { onCategorySelect, categories } = this.props
    onCategorySelect(index === 0 ? "" : categories[index - 1])
  }

  getIndex = () => {
    const { categorySelected, categories } = this.props
    return categorySelected
      ? categories.findIndex(group => group === categorySelected) + 1
      : 0
  }

  render() {
    const { categories } = this.props
    return (
      <AppBar 
      color='secondary'
      position='static'>
        <Tabs
          value={this.getIndex()}
          onChange={this.onIndexSelect}
          indicatorColor="primary"
          textColor="primary"
          centered
          //onChange={handleChange}
          //aria-label="simple tabs example"
        >
          <Tab label="All" />
          {categories.map(group => (
            <Tab key={group} label={group} />
          ))}
        </Tabs>
      </AppBar>
    )
  }
}

export default withContext(Footer)
