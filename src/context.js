import React, { Component, createContext, useEffect, useState} from "react"
import IpfsClient from 'ipfs-http-client';
import OrbitDB from 'orbit-db';


//export const { Provider, Consumer } = createContext()

export const FullContext = createContext()
export const {Provider, Consumer} = FullContext
// create context retorna un objeto con dos propiedades: Provider y Consumer
export const FULL = {Provider,Consumer} 

export const withContext = Component => props => (
  <Consumer>
  {value => <Component {...value} {...props} />}
  </Consumer>
)

//const FULL = createContext()


//high order component. instead of Provider/Consumer.
// esto seria igual a  <Component value ={...} props={...}


// export const withDatabase = Component => props=> {

//   const  [ DB , setDB ] = useState(null)

//   useEffect(() => {
//     async function init(){
//       const ipfs = IpfsClient('http://localhost:5001');
//       const orbitdb = await OrbitDB.createInstance(ipfs);
//       const db = await orbitdb.open('/orbitdb/zdpuAnWSnuMMB8HyFBU4FeXgnBwqdrozwLXT2dmKczeDRuw6t/first-database')
//       setDB(db)
//     // const db = await orbitdb.keyvalue('first-database');
//       // const bookings= await orbitdb.keyvalue('bookings')
//      //dispatch({type:'SET_DB', payload:db});
//       // dispatch({type:'SET_BOOKINGS', payload:bookings});
//       console.log(DB.address.toString())
//       const identity = DB.identity
//       console.log(identity.toJSON())
//       // console.log(bookings.address.toString())
//       // const identity1 = bookings.identity
//       // console.log(identity1.toJSON())

//     }
//   }, [])

//     return DB
  
// } 





